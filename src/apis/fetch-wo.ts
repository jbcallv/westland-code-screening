import csv from 'csvtojson';
import { WorkOrder, Departments } from '../types/types';

/**
 * Our exceptionally sophisticated "API" call to get us the
 * list of work orders processed into the system.
 * Thankfully, this call "processes" our data into a WorkOrder
 * array for us!
 */
export const fetchWorkOrders = async (customFilePath?: string) : Promise<WorkOrder[]> => {
  const fileAddress = customFilePath || 'input/work-orders.csv';

  const parseDetails = {
    colParser: {
      employeeNumber: 'number',
      part: 'number',
    },
  };

  return csv(parseDetails)
    .fromFile(fileAddress)
    .on('error', (err) => {
      // Normally we'd want to handle this case, but this is a test!
      // And we can control the environment that would cause this issue.
      console.error(err);
    });
};

/**
 * Fetches the email of the point of contact to send a given {@link WorkOrder} to depending on
 * department and part number.
 * @param department The {@link Departments} department to lookup
 * @param part The part number the work order relates to.
 */
export const getContact = (department : Departments, part: number) => {
  switch (department) {
    case 'Appliance':
      if (part < 50000) {
        return 'g.freeman@blackmesa.com';
      }
      return 'g.man@hotmail.com';
    case 'Electrical':
      if (part < 70000) {
        return 'feldspar@outerwilds.th';
      }
      return 'solanum@eye.nm';
    case 'Roofing':
      if (part < 60000) {
        return 'm.okiura@abis.go.jp';
      }
      return 'a.set@lemniscate.jp';
    case 'HVAC':
      if (part < 30000) {
        return 'g.freeman@blackmesa.com';
      }
      return 'c.johnson@aperture.science';
    case 'Plumbing':
      return 'm.mario@mushroom.kd';
    default:
      return 'it.help@westlandreg.com';
  }
};
